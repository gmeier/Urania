#!/bin/bash

export workdir="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/scripts/"
export runpath="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/"


#All data
export inputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_data.root"
export outputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_mdfit_bd.root"
export outputweightstree="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sWeights/Nominal/sWeights_RunIdata.root"
export conf="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForMDFitter_Bd.py"
export outputplotdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MDFitPlots_Bd/"
#rm -rf $outputplotdir
#mkdir -p $outputplotdir
#rm -f $outputfile
#${runpath}run python ${workdir}runMDFitter_Bd.py -d --configName $conf --inputFile $inputfile --sWeightsName $outputweightstree --mode kpipi --merge both --hypo Bd2DPi_Bd2DK --outputplotdir $outputplotdir --outputFile $outputfile --dim 1 --binned --sWeights --plotsWeights | tee ${outputplotdir}log_fit_Bd2DPi.txt

#All data, large Fit B window (systematics)
export inputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_data.root"
export outputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_mdfit_bd_largeWindow.root"
export outputweightstree="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sWeights/Nominal/sWeights_RunIdata_largeWindow.root"
export conf="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForMDFitter_Bd.py"
export outputplotdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MDFitPlots_Bd_largeWindow/"
#rm -rf $outputplotdir
#mkdir -p $outputplotdir
#rm -f $outputfile
#${runpath}run python ${workdir}runMDFitter_Bd.py -d --configName $conf --inputFile $inputfile --sWeightsName $outputweightstree --mode kpipi --merge both --hypo Bd2DPi_Bd2DK --outputplotdir $outputplotdir --outputFile $outputfile --dim 1 --binned --sWeights --plotsWeights | tee ${outputplotdir}log_fit_Bd2DPi.txt

#All data, PIDK<0 and no Bd->DK (systematics)
export inputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_data_pidk0.root"
export outputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_mdfit_bd_pidk0.root"
export outputweightstree="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sWeights/Nominal/sWeights_RunIdata_pidk0.root"
export conf="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForMDFitter_Bd_pidk0_syst.py"
export outputplotdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MDFitPlots_Bd_pidk0/"
rm -rf $outputplotdir
mkdir -p $outputplotdir
rm -f $outputfile
${runpath}run python ${workdir}runMDFitter_Bd.py -d --configName $conf --inputFile $inputfile --sWeightsName $outputweightstree --mode kpipi --merge both --hypo Bd2DPi --outputplotdir $outputplotdir --outputFile $outputfile --dim 1 --binned --sWeights --plotsWeights | tee ${outputplotdir}log_fit_Bd2DPi.txt

exit

#2011
export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_2011data_SSCombined.root"
export outputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_mdfit_bd_2011.root"
export outputweightstree="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sWeights/Nominal/sWeights_2011data.root"
export conf="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForMDFitter_Bd_verylowStat.py"
export outputplotdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MDFitPlots_Bd_2011/"
export title="2011"
rm -rf $outputplotdir
mkdir -p $outputplotdir
rm -f $outputfile
${runpath}run python ${workdir}runMDFitter_Bd.py -d --configName $conf --inputFile $inputfile --sWeightsName $outputweightstree --mode kpipi --year 2011 --merge pol --hypo Bd2DPi_Bd2DK --outputplotdir $outputplotdir --outputFile $outputfile --dim 1 --binned --sWeights --title $title --plotsWeights | tee ${outputplotdir}log_fit_Bd2DPi.txt

#2012
export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_2012data_SSCombined.root"
export outputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_mdfit_bd_2012.root"
export outputweightstree="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sWeights/Nominal/sWeights_2012data.root"
export conf="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForMDFitter_Bd_lowStat.py"
export outputplotdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MDFitPlots_Bd_2012/"
export title="2012"
rm -rf $outputplotdir
mkdir -p $outputplotdir
rm -f $outputfile
${runpath}run python ${workdir}runMDFitter_Bd.py -d --configName $conf --inputFile $inputfile --sWeightsName $outputweightstree --mode kpipi --year 2012 --merge pol --hypo Bd2DPi_Bd2DK --outputplotdir $outputplotdir --outputFile $outputfile --dim 1 --binned --sWeights --title $title --plotsWeights | tee ${outputplotdir}log_fit_Bd2DPi.txt

#MU
export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_MUdata_SSCombined.root"
export outputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_mdfit_bd_MU.root"
export outputweightstree="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sWeights/Nominal/sWeights_MUdata.root"
export conf="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForMDFitter_Bd_lowStat.py"
export outputplotdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MDFitPlots_Bd_MU/"
export title="Magnet UP"
rm -rf $outputplotdir
mkdir -p $outputplotdir
rm -f $outputfile
${runpath}run python ${workdir}runMDFitter_Bd.py -d --configName $conf --inputFile $inputfile --sWeightsName $outputweightstree --mode kpipi --pol up --merge year --hypo Bd2DPi_Bd2DK --outputplotdir $outputplotdir --outputFile $outputfile --dim 1 --binned --sWeights --title "${title}" --plotsWeights | tee ${outputplotdir}log_fit_Bd2DPi.txt

#MD
export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_MDdata_SSCombined.root"
export outputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_mdfit_bd_MD.root"
export outputweightstree="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sWeights/Nominal/sWeights_MDdata.root"
export conf="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForMDFitter_Bd_lowStat.py"
export outputplotdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MDFitPlots_Bd_MD/"
export title="Magnet DOWN"
rm -rf $outputplotdir
mkdir -p $outputplotdir
rm -f $outputfile
${runpath}run python ${workdir}runMDFitter_Bd.py -d --configName $conf --inputFile $inputfile --sWeightsName $outputweightstree --mode kpipi --pol down --merge year --hypo Bd2DPi_Bd2DK --outputplotdir $outputplotdir --outputFile $outputfile --dim 1 --binned --sWeights --title "${title}" --plotsWeights | tee ${outputplotdir}log_fit_Bd2DPi.txt

exit

#pi+
export outputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_mdfit_bd_pip.root"
export outputweightstree="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sWeights/Nominal/sWeights_pipdata.root"
export conf="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForMDFitter_Bd_lowStat.py"
export outputplotdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MDFitPlots_Bd_pip/"
export preselection="BacCharge==1"
export title="D^{-}#pi^{+}"
rm -rf $outputplotdir
mkdir -p $outputplotdir
rm -f $outputfile
${runpath}run python ${workdir}runMDFitter_Bd.py -d --configName $conf --inputFile $inputfile --sWeightsName $outputweightstree --mode kpipi --merge both --hypo Bd2DPi_Bd2DK --outputplotdir $outputplotdir --outputFile $outputfile --dim 1 --binned --sWeights --preselection $preselection --title "${title}" --plotsWeights | tee ${outputplotdir}log_fit_Bd2DPi.txt

#pi-
export outputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_mdfit_bd_pim.root"
export outputweightstree="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sWeights/Nominal/sWeights_pimdata.root"
export conf="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForMDFitter_Bd_lowStat.py"
export outputplotdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MDFitPlots_Bd_pim/"
export preselection="BacCharge==-1"
export title="D^{+}#pi^{-}"
rm -rf $outputplotdir
mkdir -p $outputplotdir
rm -f $outputfile
${runpath}run python ${workdir}runMDFitter_Bd.py -d --configName $conf --inputFile $inputfile --sWeightsName $outputweightstree --mode kpipi --merge both --hypo Bd2DPi_Bd2DK --outputplotdir $outputplotdir --outputFile $outputfile --dim 1 --binned --sWeights --preselection $preselection --title "${title}" --plotsWeights | tee ${outputplotdir}log_fit_Bd2DPi.txt