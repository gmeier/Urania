#!/bin/bash

#Prevent core dump
ulimit -c 0

#Get options
export seed=$1
export stop=$2
export input=$3
export eosoutput=$4
export nickname=$5
export massfitdescr=$6
export timefitdescr=$7
export config=$8
export pol=$9
export mode=${10}
export year=${11}
export hypo=${12}
export pyscriptpath=${13}
export runpath=${14}

export Start=`date`
echo "==> Start fitting at ${Start}"

export preselection="TagDecOS!=0||TagDecSS!=0"

while (( $seed < $stop )); do 
    
    cd $pyscriptpath
    
    #--fileName
    #sWeights_${massfitdescr}_${nickname}_${seed}.root
    #GenToyTree_${nickname}_${seed}.root

    #preselection="TMath::Abs(TrueID-100)<50"
    #preselection="TagDecOS!=0"

    ${runpath}run python ${pyscriptpath}runSFit_Bd.py --preselection $preselection --pereventmistag --randomise --NCPU 8 --HFAG --sampleConstr --noweight --toys --debug --fileName ${input}GenToyTree_${nickname}_${seed}.root --save ${eosoutput}TimeFitToysResult_${nickname}_${timefitdescr}_${massfitdescr}_${seed}.root --fileNamePull ${eosoutput}PullTreeTimeFit_${nickname}_${timefitdescr}_${massfitdescr}_${seed}.root --outputdir $eosoutput --configName $config --pol $pol --mode $mode --year $year --hypo $hypo --merge both --seed $seed >& ${eosoutput}log_${nickname}_${timefitdescr}_${massfitdescr}_${seed}.txt

    seed=$(($seed + 1))

done

export Stop=`date`
echo "==> Stop fitting at ${Stop}"