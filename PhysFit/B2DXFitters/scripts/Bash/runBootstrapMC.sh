#!/bin/bash

#Prevent core dump
ulimit -c 0

#Get options
export seed=$1
export stop=$2
export runpath=$3
export inputfile=$4
export inputworkspace=$5
export eosoutput=$6
export nickname=$7
export config=$8
export pyscriptpath=$9
export maxcand=${10}

export preselection="TagDecOS!=0||TagDecSS!=0"

while (( $seed < $stop )); do

    cd $pyscriptpath

    #Run script
    ${runpath}run python ${pyscriptpath}BootstrapMC.py -d --cheatTagging --seed $seed --preselection $preselection --configName $config --inputFile $inputfile --inputWorkspace $inputworkspace --outputFile ${eosoutput}BootstrapMC_${nickname}_${seed}.root --outputWorkspace workspace --merge both --decay Bd2DPi --mode kpipi --year run1 --hypo Bd2DPi --maxcand $maxcand >& ${eosoutput}log_${nickname}_${seed}.txt
    seed=$(($seed + 1))

done