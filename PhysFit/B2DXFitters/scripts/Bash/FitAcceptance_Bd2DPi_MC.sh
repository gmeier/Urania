#!/bin/bash

export workdir="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/scripts/"
export rundir="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/"
export outputplotdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/MCAcceptance/"
export configfile="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForMCAcceptance_New2.py"

export inputfile="[root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DPi_3fb/MC/MCfiltered_Bd2DPi_Bd2DPiHypo_magUp_2011_S21_afterSelection_SSMCtrained_SSandOSCombined_PID-corrected_new.root;root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DPi_3fb/MC/MCfiltered_Bd2DPi_Bd2DPiHypo_magDown_2011_S21_afterSelection_SSMCtrained_SSandOSCombined_PID-corrected_new.root;root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DPi_3fb/MC/MCfiltered_Bd2DPi_Bd2DPiHypo_magUp_2012_S21_afterSelection_SSMCtrained_SSandOSCombined_PID-corrected_new.root;root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DPi_3fb/MC/MCfiltered_Bd2DPi_Bd2DPiHypo_magDown_2012_S21_afterSelection_SSMCtrained_SSandOSCombined_PID-corrected_new.root]"

rm -rf $outputplotdir
mkdir -p $outputplotdir

#All MC tagged
export inputtree="Bd2Dpi"
export nickname="All_Tag"
export LHCbText="LHCb Simulation"
${rundir}run python ${workdir}FitAcceptance.py --configName $configfile --inputfile $inputfile --inputtree $inputtree --outputplotdir $outputplotdir --nickname $nickname --LHCbText "${LHCbText}" | tee ${outputplotdir}logfile_${nickname}.txt

exit

#All MC, tagged, overlap of all triggers
export preselection="lab0_Hlt2Topo2BodyBBDTDecision_TOS==1&&(lab0_Hlt2Topo3BodyBBDTDecision_TOS==1||lab0_Hlt2Topo4BodyBBDTDecision_TOS==1)" 
export inputtree="Bd2Dpi"
export nickname="All_Tag_overlap"
export LHCbText="LHCb Simulation TOS overlap"
${rundir}run python ${workdir}FitAcceptance.py --configName $configfile --inputfile $inputfile --inputtree $inputtree --outputplotdir $outputplotdir --nickname $nickname --LHCbText "${LHCbText}" --preselection $preselection | tee ${outputplotdir}logfile_${nickname}.txt

#All MC, tagged, 2B TOS
export preselection="lab0_Hlt2Topo2BodyBBDTDecision_TOS==1&&lab0_Hlt2Topo3BodyBBDTDecision_TOS==0&&lab0_Hlt2Topo4BodyBBDTDecision_TOS==0"
export inputtree="Bd2Dpi"
export nickname="All_Tag_2B"
export LHCbText="LHCb Simulation Tagged 2BodyTOS"
${rundir}run python ${workdir}FitAcceptance.py --configName $configfile --inputfile $inputfile --inputtree $inputtree --outputplotdir $outputplotdir --nickname $nickname --LHCbText "${LHCbText}" --preselection $preselection | tee ${outputplotdir}logfile_${nickname}.txt

#All MC, tagged, 34B TOS
export preselection="lab0_Hlt2Topo2BodyBBDTDecision_TOS==0&&(lab0_Hlt2Topo3BodyBBDTDecision_TOS==1||lab0_Hlt2Topo4BodyBBDTDecision_TOS==1)"
export inputtree="Bd2Dpi"
export nickname="All_Tag_34B"
export LHCbText="LHCb Simulation Tagged 34BodyTOS"
${rundir}run python ${workdir}FitAcceptance.py --configName $configfile --inputfile $inputfile --inputtree $inputtree --outputplotdir $outputplotdir --nickname $nickname --LHCbText "${LHCbText}" --preselection $preselection | tee ${outputplotdir}logfile_${nickname}.txt
