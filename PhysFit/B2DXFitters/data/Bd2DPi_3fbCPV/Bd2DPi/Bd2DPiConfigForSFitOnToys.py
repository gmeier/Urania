def getconfig() :

    import math
    from math import pi

    configdict = {}

    configdict["Decay"] = "Bd2DPi"

    ############################################
    # Define all basic variables
    ############################################

    configdict["BasicVariables"] = {}

    configdict["BasicVariables"]["BeautyTime"]    = { "Range"                  : [0.4,     12.0    ],
                                                      "Bins"                   : 40,
                                                      "Name"                   : "BeautyTime",
                                                      "InputName"              : "lab0_FitDaughtersPVConst_ctau_flat"}

    configdict["BasicVariables"]["BacCharge"]     = { "Range"                  : [-1000.0, 1000.0  ],
                                                      "Name"                   : "BacCharge",
                                                      "InputName"              : "lab1_ID"}

    configdict["BasicVariables"]["TagDecOS"]      = { "Range"                  : [-1.0,    1.0     ],
                                                      "Name"                   : "TagDecOS",
                                                      "InputName"              : "lab0_TAGDECISION_OS"}

    configdict["BasicVariables"]["TagDecSS"]      = { "Range"                  : [-1.0,    1.0     ],
                                                      "Name"                   : "TagDecSS",
                                                      "InputName"              : "lab0_SS_PionBDT_DEC"}

    configdict["BasicVariables"]["MistagOS"]      = { "Range"                  : [ 0.0,  0.5 ],
                                                      "Name"                   : "MistagOS",
                                                      "InputName"              : "lab0_TAGOMEGA_OS"}

    configdict["BasicVariables"]["MistagSS"]      = { "Range"                  : [ 0.0,    0.5     ],
                                                      "Name"                   : "MistagSS",
                                                      "InputName"              : "lab0_SS_PionBDT_PROB"}

    ############################################
    # Define all CPV and decay rate parameters
    ############################################

    #Parameters from https://svnweb.cern.ch/trac/lhcb/browser/DBASE/tags/Gen/DecFiles/v27r42/dkfiles/Bd_D-pi+,Kpipi=CPVDDalitz,DecProdCut.dec)
    ModqOverp_d     =  1
    ArgqOverp_d     =  -0.746
    ModpOverq_d     =  1
    ArgpOverq_d     =  0.746
    ModAf_d         =  0.0849
    ArgAf_d         =  0.002278
    ModAbarf_d      =  0.00137
    ArgAbarf_d      =  -1.128958
    ModAfbar_d      =  0.00137
    ArgAfbar_d      =  1.3145
    ModAbarfbar_d   =  0.0849
    ArgAbarfbar_d   =  0.002278

    configdict["ACP"] = {}
    configdict["ACP"]["Signal"] = { "Gamma"                : [1.0/1.518, 0.0, 2.0],
                                    "DeltaGamma"           : [0.0],
                                    "DeltaM"               : [0.5050, 0.01, 2.0],#[0.510, 0.01, 2.0],#[0.5050, 0.01, 2.0], #Global average from HFAG (http://www.slac.stanford.edu/xorg/hfag/osc/summer_2016/)
                                    #"ArgLf"                : [ArgqOverp_d + ArgAbarf_d - ArgAf_d],
                                    #"ArgLbarfbar"          : [ArgpOverq_d + ArgAfbar_d - ArgAbarfbar_d],
                                    #"ModLf"                : [ModAbarf_d/ModAf_d],
                                    "C"                    : [1.0], #we neglect r^2 terms
                                    "S"                    : [-0.031], #from decfile
                                    "Sbar"                 : [-0.029], #from decfile
                                    "D"                    : [0.0], #from DeltaGamma=0
                                    "Dbar"                 : [0.0], #from DeltaGamma=0
                                    "ParameteriseIntegral" : True,
                                    "CPlimit"              : {"upper":1.0, "lower":-1.0},
                                    "NBinsAcceptance"      : 0} #keep at zero if using spline acceptance!

    ############################################
    # Define resolution and acceptance models
    ############################################

    configdict["ResolutionAcceptance"] = {}
    configdict["ResolutionAcceptance"]["Signal"] = {}
    configdict["ResolutionAcceptance"]["Signal"] = {"TimeErrorPDF": None,
                                                    "Acceptance": #From ANA note v2
                                                    {"Type": "Spline",
                                                     "Float": True,
                                                     "Extrapolate": True,
                                                     "KnotPositions" : [0.5, 1.0, 1.5, 2.0, 2.3, 2.6, 3.0, 4.0, 10.0],
                                                     "KnotCoefficients" : [1.9440e-01, 3.3275e-01, 6.1444e-01, 8.6628e-01, 9.9600e-01, 1.0745e+00, 1.1083e+00, 1.1565e+00, 1.1946e+00]},
                                                    "Resolution": #From ANA note v2
                                                    {"Type": "AverageModel",
                                                     "Parameters": { 'sigmas': [ 0.05491 ], 'fractions': [] },
                                                     "Bias": [0.0],
                                                     "ScaleFactor": [1.0]}
                                                    }

    ############################################
    # Define asymmetries
    ############################################

    configdict["ProductionAsymmetry"] = {}
    configdict["DetectionAsymmetry"] = {}
    configdict["ProductionAsymmetry"]["Signal"] = {}
    configdict["DetectionAsymmetry"]["Signal"] = {}
    configdict["ProductionAsymmetry"]["Signal"] = [-0.00638385, -1.0, 1.0]#fitted on data #[0.0] #[-0.0124, -1.0, 1.0] #from ANA note v2
    configdict["DetectionAsymmetry"]["Signal"] = [0.0086, -1.0, 1.0]#fitted on data #[0.0086, -1.0, 1.0] #from arXiv:1408.0275v2 (OPPOSITE SIGN!!!)

    ############################################
    # Define taggers and their calibration
    ############################################

    configdict["Taggers"] = {}
    configdict["Taggers"]["Signal"] = {}
    configdict["Taggers"]["Signal"] = {"OS" :
                                       {"Calibration":
                                        { "Type" : "Linear",
                                          "p0"  : [0.37, 0.0, 5.0],
                                          "p1"  : [0.8, 0.0, 5.0],
                                          "deltap0" : [0.0017216, -2.0, 2.0],
                                          "deltap1" : [0.051919, -2.0, 2.0],
                                          "avgeta" : [0.37],
                                          "tageff"   : [0.4, 0.0, 1.0],
                                          "tagasymm" : [0.0]
                                          },
                                        "MistagPDF" :
                                        {"Type"     : "BuildTemplate"}
                                        },
                                       "SS" :
                                       {"Calibration":
                                        { "Type" : "Linear",
                                          "p0"  : [0.4, 0.0, 5.0],
                                          "p1"  : [0.8, 0.0, 5.0],
                                          "deltap0" : [0.0017216, -2.0, 2.0],
                                          "deltap1" : [0.051919, -2.0, 2.0],
                                          "avgeta" : [0.4],
                                          "tageff"   : [0.9, 0.0, 1.0],
                                          "tagasymm" : [0.0]
                                          },
                                        "MistagPDF" :
                                        {"Type"     : "BuildTemplate"}
                                        }
                                       }
    #configdict["Taggers"]["Signal"] = {"OS" :
    #                                   {"Calibration":
    #                                    { "Type": "GLM",
    #                                      "XML": ["/eos/lhcb/wg/b2oc/TD_DPi_3fb/calibrations/RLogisticCalibration_Bu2D0Pi_OS_20171109.xml"],
    #                                      "tageff"   : [0.432389, 0.0, 1.0],
    #                                      "tagasymm" : [0.0]
    #                                      },
    #                                    "MistagPDF" :
    #                                    {"Type"     : "BuildTemplate"}
    #                                    },
    #                                   "SS":
    #                                   {"Calibration":
    #                                    { "Type": "GLM",
    #                                      "XML": ["/eos/lhcb/wg/b2oc/TD_DPi_3fb/calibrations/RLogisticCalibration_Bd2JpsiKst_SS_20171101.xml"],
    #                                      "tageff"   : [0.930458, 0.0, 1.0],
    #                                      "tagasymm" : [0.0]
    #                                      },
    #                                    "MistagPDF" :
    #                                    {"Type"     : "BuildTemplate"}
    #                                    }
    #                                   }
    
    ############################################
    # Choose parameters to fix
    ############################################

    configdict["constParams"] = []
    configdict["constParams"].append('Cf')
    configdict["constParams"].append('Cfbar')
    configdict["constParams"].append('Df')
    configdict["constParams"].append('Dfbar')
    configdict["constParams"].append('.*scalefactor')
    configdict["constParams"].append('resmodel00_sigma')

    ############################################
    # Build gaussian constraints
    # See B2DXFitters/GaussianConstraintBuilder.py for documentation
    ############################################

    configdict["gaussCons"] = {}
    # Constraint on resolution
    # Constraint on DeltaM
    configdict["gaussCons"]["deltaM"] = math.sqrt(0.0021*0.0021 + 0.0010*0.0010)
    # Constraint on Gamma (error on gamma = rel. error on lifetime * gamma)
    configdict["gaussCons"]["Gamma"] = (0.004/1.518) * (1.0/1.518)

    ############################################
    # Choose parameters to perform the
    # likelihood scan for
    ############################################

    configdict["LikelihoodScan"] = []
    configdict["LikelihoodScan"].append("Sf")
    configdict["LikelihoodScan"].append("Sfbar")

    ############################################
    # Choose initial free parameters to randomise
    ############################################

    configdict["randomiseParams"] = {}
    configdict["randomiseParams"] = {'Sf'                          : {'min': -0.5,   'max': -0.0001},
                                     'Sfbar'                       : {'min': -0.5, 'max': -0.0001},
                                     #'Cf'                          : {'min': 0.1,   'max': 1.0},
                                     'ProdAsymm'                   : {'min': -0.5,   'max': -0.0001},
                                     'DetAsymm'                    : {'min': 0.0001,   'max': 0.5}
                                     }

    return configdict
