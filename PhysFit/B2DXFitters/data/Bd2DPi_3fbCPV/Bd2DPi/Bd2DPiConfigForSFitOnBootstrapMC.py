"""configuration file for decaytime fit on signal bootstrapped MC"""
def getconfig():

    import math
    from math import pi

    configdict = {}

    configdict["Decay"] = "Bd2DPi"

    ############################################
    # Define all basic variables
    ############################################

    configdict["BasicVariables"] = {}

    configdict["BasicVariables"]["BeautyTime"]    = {"Range": [0.4, 12.0],
                                                     "Bins": 40,
                                                     "Name": "BeautyTime",  # the name is not used by the MDFitterSettings/Translator etc.
                                                     "InputName": "obsTime"}
    # if one wants to cut on the dataset the easiest way is to add a preselection in addition to the range defined here (preselection added
    # by option --preselection on command line)

    configdict["BasicVariables"]["BacCharge"]     = {"Range": [-1000.0, 1000.0],
                                                     "Name": "BacCharge",
                                                     "InputName": "BacCharge"}

    #configdict["BasicVariables"]["TagDecTrue"]      = {"Range": [-1.0, 1.0],
    #                                                   "Name": "TagDecOS",
    #                                                   "InputName": "TagDecTrue"}

    configdict["BasicVariables"]["TagDecOS"]      = {"Range": [-1.0, 1.0],
                                                     "Name": "TagDecOS",
                                                     "InputName": "TagDecOS"}

    #configdict["BasicVariables"]["TagDecCheat"]      = {"Range": [-1.0, 1.0],
    #                                                    "Name": "TagDecCheat",
    #                                                    "InputName": "TagDecCheat"}

    configdict["BasicVariables"]["TagDecSS"]      = {"Range": [-1.0, 1.0],
                                                     "Name": "TagDecSS",
                                                     "InputName": "TagDecSS"}  # available via mdSet->GetTagVar(i)
    # the original specifier is available via mdSet->GetTagVarOutName(i)
    # this name + "_idx" is used in the inputtree/dataset - so no flexibility in the beginning of the name as "TagDec" is used when checking for
    # tagging variables in general at the beginning

    configdict["BasicVariables"]["MistagOS"]      = {"Range": [0.0, 0.5],  # 0.4619
                                                     "Name": "MistagOS",
                                                     "InputName": "MistagOS"}

    #configdict["BasicVariables"]["MistagCheat"]      = {"Range": [0.0, 0.5],
    #                                                    "Name": "MistagCheat",
    #                                                    "InputName": "MistagCheat"}

    configdict["BasicVariables"]["MistagSS"]      = {"Range": [0.0, 0.5],  # 0.492
                                                     "Name": "MistagSS",
                                                     "InputName": "MistagSS"}

    configdict["AdditionalVariables"] = {}
    #configdict["AdditionalVariables"]["TrueID"]   = {"Range": [0.0, 1500.0],
    #                                                 "InputName": "TrueID"}

    ############################################
    # Define all CPV and decay rate parameters
    ############################################

    # Parameters from https://svnweb.cern.ch/trac/lhcb/browser/DBASE/tags/Gen/DecFiles/v27r42/dkfiles/Bd_D-pi+,Kpipi=CPVDDalitz,DecProdCut.dec)
    ModqOverp_d     = 1
    ArgqOverp_d     = -0.746
    ModpOverq_d     = 1
    ArgpOverq_d     = 0.746
    ModAf_d         = 0.0849
    ArgAf_d         = 0.002278
    ModAbarf_d      = 0.00137
    ArgAbarf_d      = -1.128958
    ModAfbar_d      = 0.00137
    ArgAfbar_d      = 1.3145
    ModAbarfbar_d   = 0.0849
    ArgAbarfbar_d   = 0.002278

    configdict["ACP"] = {}
    configdict["ACP"]["Signal"] = {"Gamma": [1.0 / 1.519068, 0.1, 2.0],
                                   "DeltaGamma": [0.0],
                                   "DeltaM": [0.510, 0.01, 5.0],
                                   #"ArgLf": [ArgqOverp_d + ArgAbarf_d - ArgAf_d],
                                   #"ArgLbarfbar": [ArgpOverq_d + ArgAfbar_d - ArgAbarfbar_d],
                                   #"ModLf": [ModAbarf_d / ModAf_d],
                                   "C": [1.0],  # we neglect r^2 terms
                                   "S": [-0.031],  # from decfile
                                   "Sbar": [-0.029],  # from decfile
                                   "D": [0],  # from DeltaGamma=0
                                   "Dbar": [0],  # from DeltaGamma=0
                                   "ParameteriseIntegral": True,
                                   "CPlimit": {"upper": 2.0, "lower": -2.0},
                                   "NBinsAcceptance": 0}  # keep at zero if using spline acceptance!

    ############################################
    # Define resolution and acceptance models
    ############################################

    configdict["ResolutionAcceptance"] = {}
    configdict["ResolutionAcceptance"]["Signal"] = {}
    configdict["ResolutionAcceptance"]["Signal"] = {"TimeErrorPDF": None,
                                                    "Acceptance": # From ANA note v2
                                                    {"Type": "Spline",
                                                     "Float": True,
                                                     "Extrapolate": True,
                                                     #"ToFix": [3],
                                                     #"KnotPositions" : [0.45, 1.0, 2.0, 2.5, 7.0, 12.0],
                                                     #"KnotCoefficients": [0.5, 0.7, 1.0, 0.9, 0.85, 0.7, 0.7]},
                                                     "KnotPositions": [0.5, 1.0, 1.5, 2.0, 2.3, 2.6, 3.0, 4.0, 10.0],
                                                     "KnotCoefficients": [1.9440e-01, 3.3275e-01, 6.1444e-01, 8.6628e-01, 9.9600e-01, 1.0745e+00, 1.1083e+00,  1.1565e+00, 1.1946e+00]},
                                                    "Resolution":  # From ANA note v2
                                                    {"Type": "AverageModel",
                                                     "Parameters": {'sigmas': [0.05491], 'fractions': []},
                                                     "Bias": [0.0],
                                                     "ScaleFactor": [1.0]}
                                                    }

    ############################################
    # Define asymmetries
    ############################################

    configdict["ProductionAsymmetry"] = {}
    configdict["DetectionAsymmetry"] = {}
    configdict["ProductionAsymmetry"]["Signal"] = {}
    configdict["DetectionAsymmetry"]["Signal"] = {}
    configdict["ProductionAsymmetry"]["Signal"] = [-0.0124, -1.0, 1.0]  # from ANA note v2
    configdict["DetectionAsymmetry"]["Signal"] = [0.0086, -1.0, 1.0]  # from arXiv:1408.0275v2 (OPPOSITE SIGN!!!)

    ############################################
    # Define taggers and their calibration
    ############################################

    configdict["Taggers"] = {}
    configdict["Taggers"]["Signal"] = {}
    configdict["Taggers"]["Signal"] = {"OS":  # From Bu MC, stat and syst combined
                                       {"Calibration":
                                        {"Type": "GLM",
                                         "XML": ["/eos/lhcb/wg/b2oc/TD_DPi_3fb/calibrations/RLogisticCalibration_MCTruth_Bu2D0Pi_OS_20171114.xml"],
                                         "tageff": [0.45, 0.01, 0.99],  # float in the fit
                                         "tagasymm": [0.0]
                                         },
                                        "MistagPDF":
                                        {"Type": "BuildTemplate"}
                                        },
                                       "SS":
                                       {"Calibration":
                                        {"Type": "GLM",
                                         "XML": ["/eos/lhcb/wg/b2oc/TD_DPi_3fb/calibrations/RLogisticCalibration_MCTruth_Bd2JpsiKst_FullReweightAligned_SS_20171116.xml"],
                                         "tageff": [0.92, 0.01, 0.99],  # float in the fit
                                         "tagasymm": [0.0]
                                         },
                                        "MistagPDF":
                                        {"Type": "BuildTemplate"}
                                        }
                                       }
                                       

    ############################################
    # Choose parameters to fix
    ############################################

    configdict["constParams"] = []
    configdict["constParams"].append('Cf')
    configdict["constParams"].append('Cfbar')
    #configdict["constParams"].append('Sf')
    #configdict["constParams"].append('Sfbar')
    configdict["constParams"].append('Df')
    configdict["constParams"].append('Dfbar')
    configdict["constParams"].append('.*scalefactor')
    configdict["constParams"].append('resmodel00_sigma')

    ############################################
    # Build gaussian constraints
    # See B2DXFitters/GaussianConstraintBuilder.py for documentation
    ############################################

    configdict["gaussCons"] = {}
    # Constraint on DeltaM
    configdict["gaussCons"]["deltaM"] = math.sqrt(0.0021 * 0.0021 + 0.0010 * 0.0010)  # 0.0023259
    # Constraint on Gamma (error on gamma = rel. error on lifetime * gamma)
    configdict["gaussCons"]["Gamma"] = (0.004 / 1.519068) * (1.0 / 1.519068)  # 0.0017313

    return configdict
