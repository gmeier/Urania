"""
Usage:
    python makeSmallTree.py <input file>

    This macro takes the gigantic TTree with J/psi candidates and reduces
    it using only the variables which are needed. Some additinal variables
    are computed on the fly.
    The macro does not apply any cuts, in order to be able to use
    the small tree as friend of the big tree if needed.

    The difference with the non-"_auto.py" version is that this macro makes
    the small tree from the many files that would comprise a big one.
    """

    ######################
def addFloat(outT,thename):
    """
    Add a float to tree. Saves one line of code per call.
    """
    from array import array
    newF = array( 'f', [ 0. ] )
    outT.Branch( thename, newF, thename+"/f" )
    return newF

######################
def writeTree(inT,n):
    """
    Write the tree
    https://root.cern.ch/how/how-write-ttree-python
    """
    from math import log, sqrt
    
    outT = TTree( 'Jpsi2ee', 'Small Tree' )
    event      = addFloat(outT,"event")        # event and run are there to test Friend
    run        = addFloat(outT,"run")
    mass       = addFloat(outT,"mass")
    pt         = addFloat(outT,"pt")           # J/psi pt
    y          = addFloat(outT,"y")            # J/psi rapidity
    pseudoT    = addFloat(outT,"pseudoT")
#    bkgcat     = addFloat(outT,"bkgcat")      # Background category
#    dtfM       = addFloat(outT,"dtfM")        # The mass from decayTreeFitter
#    dtfChi2    = addFloat(outT,"dtfChi2")     # The chi2 from decayTreeFitter
    epPT       = addFloat(outT,"epPT")         # e+ PT
    emPT       = addFloat(outT,"emPT")
    epTrackPT  = addFloat(outT,"epTrackPT")    # before brem
    emTrackPT  = addFloat(outT,"emTrackPT")
    epPe       = addFloat(outT,"epPe")         # electron PID
    emPe       = addFloat(outT,"emPe")
    epBrem     = addFloat(outT,"epBrem")       # brem categories
    emBrem     = addFloat(outT,"emBrem")
    pt_asym    = addFloat(outT,"pt_asym")      # asymmetry of pt
#    p_asym     = addFloat(outT,"p_asym")
    spd        = addFloat(outT,"spd")          # number of SPD hits
    PV         = addFloat(outT,"PV")           # number of PVs
    polarity   = addFloat(outT,"polarity")     # Magnet
    epL0CaloET = addFloat(outT,"epL0CaloET")   # L0Calo ECAL real ET from the e+ track
    emL0CaloET = addFloat(outT,"emL0CaloET")   
    ep_bremadd = addFloat(outT,"ep_bremadd")   # e+ has bremsstrahlung added
    em_bremadd = addFloat(outT,"em_bremadd")
    ep_TrackP  = addFloat(outT,"ep_TrackP")
    em_TrackP  = addFloat(outT,"em_TrackP")
    ep_TrackPX  = addFloat(outT,"ep_TrackPX")
    em_TrackPX  = addFloat(outT,"em_TrackPX")
    ep_TrackPY  = addFloat(outT,"ep_TrackPY")
    em_TrackPY  = addFloat(outT,"em_TrackPY")
    ep_TrackPZ  = addFloat(outT,"ep_TrackPZ")
    em_TrackPZ  = addFloat(outT,"em_TrackPZ")
#     = addFloat(outT,"")
#   TIS TOS
    psi_l0edecisionTOS           = addFloat(outT,"psi_l0edecisionTOS")
    psi_l0globalTIS              = addFloat(outT,"psi_l0globalTIS")
    psi_hlt1singenoIPdecisionTOS = addFloat(outT,"psi_hlt1singenoIPdecisionTOS")
    psi_hlt1trackmvadecisionTOS  = addFloat(outT,"psi_hlt1trackmvadecisionTOS")
    psi_hlt1physTIS              = addFloat(outT,"psi_hlt1physTIS")

    if ((not n>0) or n>inT.GetEntries()): n = inT.GetEntries()
    
#    frac = 1000 # print frequency
    pc = 0
    for i in range(n):
        lastpc = pc
        pc = int(100.*i/n+0.5)
        inT.GetEntry(i)
        run[0]        = inT.runNumber
        event[0]      = inT.eventNumber
        mass[0]       = inT.Psi_M
#        bkgcat[0]     = inT.Psi_BKGCAT
        pt[0]         = inT.Psi_PT
        y[0]          = 0.5*log((inT.Psi_PE+inT.Psi_PZ)/(inT.Psi_PE-inT.Psi_PZ))
# rapidity https://en.wikipedia.org/wiki/Rapidity#In_experimental_particle_physics
        pseudoT[0]    = (inT.Psi_ENDVERTEX_Z-inT.Psi_OWNPV_Z)*mass[0]/inT.Psi_PZ
#        dtfM[0]       = inT.Psi_PVFit_M[0]    # !BUGGY! This is an array (running over PVs)
#        dtfChi2[0]    = inT.Psi_PVFit_chi2[0] # !BUGGY! This is an array (running over PVs)
        epPT[0]       = inT.eplus_PT
        emPT[0]       = inT.eminus_PT
        epTrackPT[0]  = sqrt((inT.eplus_TRACK_Tx/inT.eplus_TRACK_qOverP)**2+(inT.eplus_TRACK_Ty/inT.eplus_TRACK_qOverP)**2)
        emTrackPT[0]  = sqrt((inT.eminus_TRACK_Tx/inT.eminus_TRACK_qOverP)**2+(inT.eminus_TRACK_Ty/inT.eminus_TRACK_qOverP)**2)
        epPe[0]       = inT.eplus_MC15TuneV1_ProbNNe
        emPe[0]       = inT.eminus_MC15TuneV1_ProbNNe
        epBrem[0]     = inT.eplus_BremMultiplicity
        emBrem[0]     = inT.eminus_BremMultiplicity
        pt_asym[0]    = (inT.eplus_PT-inT.eminus_PT)/(inT.eplus_PT+inT.eminus_PT)
#        p_asym[0]     = (inT.eplus_P-inT.eminus_P)/(inT.eplus_P+inT.eminus_P)
        spd[0]        = inT.nSPDHits
        PV[0]         = inT.nPVs
        polarity[0]   = inT.Polarity
        epL0CaloET[0] = inT.eplus_L0Calo_ECAL_realET
        emL0CaloET[0] = inT.eminus_L0Calo_ECAL_realET
        ep_bremadd[0] = inT.eplus_HasBremAdded
        em_bremadd[0] = inT.eminus_HasBremAdded
        ep_TrackP[0]  = 1./inT.eplus_TRACK_qOverP
        em_TrackP[0]  = -1./inT.eminus_TRACK_qOverP
        ep_TrackPX[0] = inT.eplus_TRACK_Tx/inT.eplus_TRACK_qOverP
        em_TrackPX[0] = inT.eminus_TRACK_Tx/inT.eminus_TRACK_qOverP
        ep_TrackPY[0] = inT.eplus_TRACK_Ty/inT.eplus_TRACK_qOverP
        em_TrackPY[0] = inT.eminus_TRACK_Ty/inT.eminus_TRACK_qOverP
        ep_TrackPZ[0] = sqrt((1./inT.eplus_TRACK_qOverP)**2-(inT.eplus_TRACK_Tx/inT.eplus_TRACK_qOverP)**2-(inT.eplus_TRACK_Ty/inT.eplus_TRACK_qOverP)**2)
        em_TrackPZ[0] = sqrt((-1./inT.eminus_TRACK_qOverP)**2-(inT.eminus_TRACK_Tx/inT.eminus_TRACK_qOverP)**2-(inT.eminus_TRACK_Ty/inT.eminus_TRACK_qOverP)**2)
        psi_l0edecisionTOS[0]           = inT.Psi_L0ElectronDecision_TOS
        psi_l0globalTIS[0]              = inT.Psi_L0Global_TIS
        psi_hlt1singenoIPdecisionTOS[0] = inT.Psi_Hlt1SingleElectronNoIPDecision_TOS
        psi_hlt1trackmvadecisionTOS[0]  = inT.Psi_Hlt1TrackMVADecision_TOS
        psi_hlt1physTIS[0]              = inT.Psi_Hlt1Phys_TIS
        
        if (i==0 or i==n or pc>lastpc): print "Candidate",i,"of",n,"(",pc,"%) with mass",mass[0]
        outT.Fill()
    return outT
###############################################
"""
``Main''
"""
from AllMacroes import *
from ROOT import *
import sys
import os
"""if ( len(sys.argv) == 2 ):
    inFile = sys.argv[1]
    n = 0
elif ( len(sys.argv) == 3 ):
    inFile = sys.argv[1]
    n = int(sys.argv[2])
else :
    print "Usage: python makeSmallTree.py <input file> [ <number-of-events> ]"
    sys.exit()
"""
for num in range(0,61):
    f=TFile.Open("../patrick/Jpsi2017-1566-{0}.root".format(num))
    f.ls()
    t=f.Get("Hlt2DiElectronJPsiEETurbo_Tuple/DecayTree")  # The TTree
    t.Show(0) # shows all variables
    n = t.GetEntries()
    # print n

    fout = TFile( '../v3/Jpsi2017-1566-{0}-Small.root'.format(num), 'recreate' )
    fout.cd()
    newT = writeTree(t,n)
    newT.Write()
    fout.Close()

