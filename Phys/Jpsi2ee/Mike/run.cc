#include <iostream>
#include "data.h"

using namespace std;

// fiducial region for tracks
// these are Hlt1 settings from pp_2017
bool fid(Data &data, int i){
  TLorentzVector p4 = data.getPrtP4(i);
  if(p4.Pt() < 600 || p4.P() < 5000) return false;
  return true;
}

// find closest L0 electron cluster to track p using track state at ECAL face
// p: particle index
// min: smallest relative distance found
// r: E/p
// x,y: location of the nearest cluster
// idx: index of the nearest cluster
void closestEcal(Data &data, int p, double &min, double &r, double &x,
  double &y, int &idx){
  min = 1e9; r = -1; idx=-1;
  int nl0 = data.nl0();
  TLorentzVector p4 = data.getPrtP4(p);
  for(int i=0; i<nl0; i++){
    if(data.l0_id->at(i) != 0) continue; // 0 = L0Electron
    double d = data.getPrtL0RelDist(p,i);
    double e = data.getL0energy(i);
    if(d < min) {
      idx = i;
      min = d; r = e / p4.P();
      x = data.l0_x->at(i); y = data.l0_y->at(i);
    }
  }
}

// primitive L0-based electron ID
// return 0 = fail, 1 = passes looose, 2 = passes tight
int passL0EID(Data &data, int p, int &idx){
  TLorentzVector p4 = data.getPrtP4(p);
  double d,r,x,y;
  closestEcal(data,p,d,r,x,y,idx);
  bool pass = d < 3 && r > 0.6 && r < 1.0;
  // account for ECAL saturtion
  if(p4.Pt() > 8000) pass = d < 3 && r*p4.Pt() > 5000;
  if(pass) return 2;
  // very loose selection (bascially any cluster found nearby)
  if(d < 5 && r*p4.Pt() > 500) return 1;
  return 0;
}

// extrapolate track direction to ECAL, look for photon or electron cluster
// does not consider idx0 and idx1, which are assumed to be the clusters
// associated to the e+ and e- track states themselves at the ECAL
double findBrem(Data &data, int p, int idx0, int idx1, double &min){
  int nl0 = data.nl0();
  min=1e9;
  double ebrem = 0;
  for(int i=0; i<nl0; i++){
    if(i == idx0 || i == idx1) continue;
    if(data.l0_id->at(i) > 1) continue; // 0 = electron, 1 = photon
    double d = data.getPrtVeloL0RelDist(p,i);
    double e = data.getL0energy(i);
    if(d < min) {
      min = d; ebrem = e;
    }
  }
  if(min < 3) return ebrem;
  else return 0;
}

// find min distance of L0 cluster i to particles p0 and p1, considering both
// the track states at the ECAL -AND- the velo projections to the ECAL
double ecalDist(Data &data, int i, int p0, int p1){
  if(data.l0_id->at(i) > 1) return -1; // not electron or photon cluster
  double d = data.getPrtL0RelDist(p0,i);
  double min = d;
  d = data.getPrtVeloL0RelDist(p0,i);
  if(d < min) min = d;
  d = data.getPrtL0RelDist(p1,i);
  if(d < min) min = d;
  d = data.getPrtVeloL0RelDist(p1,i);
  if(d < min) min = d;
  return min;
}

// hp/ha with proper binomial errors (including when the ratio is 1)
void divide(TH1F &hp, TH1F &ha){
  int nb = ha.GetNbinsX();
  for(int b=1; b<=nb; b++){
    int na = ha.GetBinContent(b);
    if(na < 1){
      hp.SetBinContent(b,0);
      hp.SetBinError(b,0);
    }
    else{
      int np = hp.GetBinContent(b);
      double p = np/(double)na;
      double pe = sqrt(p*(1-p)/na);
      if(np == na) {
        double ptmp = (np-1)/(double)na;
        pe = sqrt(ptmp*(1-ptmp)/na);
      }
      hp.SetBinContent(b,p);
      hp.SetBinError(b,pe);
    }
  }
}

int main(int argc, char *argv[]){

  // usage is run 0 for data and run 1 for mc
  bool mc=atoi(argv[1]);

  // histograms
  TH1F hd("hd","rel distance from track state at ecal to nearest L0 cluster",100,0,100);
  TH1F hr("hr","L0 cluster energy / track momentum",100,0,10);
  TH2F hdr("hdr","r vs d",100,0,100,100,0,10);
  TH1F hm("hm","mass no brem",1000,0,10000);
  TH1F hmb("hmb","mass with brem",1000,0,10000);
  TH2F hxy("hxy","x-y locations of matched L0 clusters",800,-4000,4000,800,-4000,4000);
  TH1F hdb("hdb","rel distance from velo projection to ecal to nearest L0 cluster",100,0,50);
  TH1F hmeeg("hmeeg","e+e-gamma mass using unsed L0 photon clusters for gamma",1000,0,10000);
  int nptbins = 9;
  double ptbins[10] = {0,0.5,1,1.5,2,3,5,7,10,25};
  TH1F hpt("hpt","all electrons",nptbins,ptbins);
  TH1F hptp("hptp","electrons passing tight e PID",nptbins,ptbins);
  TH1F hptp2("hptp2","electrons passing loose e PID",nptbins,ptbins);
  TH1F hjpt("hjpt","all jpsi",nptbins,ptbins);
  TH1F hjptp("hjptp","jpsi with both electrons passing tight",nptbins,ptbins);
  TH1F hjptp2("hjptp2","jpsi with at least one e passing tight",nptbins,ptbins);

  TFile *f;
  if(mc) f = new TFile("DiElectron.MC16.MD.24152001.root");
  else f = new TFile("DiElectron.CL16.MD.Reco16MINIBIAS.root");
  TTree *t = (TTree*)f->Get("data");
  Data data(mc);
  data.init(t);
  int nentries = t->GetEntries();
  int nL0hadron=0; // use this to normalize rates
  // various counters, definitions changing rapidly
  int n1_pass=0,n2_pass=0,n3_pass=0;

  // loop over events
  for(int e=0; e<nentries; e++){
    t->GetEntry(e);

    // GEC 
    //if(data.spd > 450) continue;

    // grab max L0 cluster info for event
    double l0h_max=0, l0e_max=0, l0p_max=0;
    int nl0 = data.nl0();
    for(int i=0; i<nl0; i++){
      int id = data.l0_id->at(i);
      double et = data.l0_et->at(i);
      if(id == 0){ // L0Electron
        if(et > l0e_max) l0e_max = et;
      }
      else if(id == 1){ // L0Photon
        if(et > l0p_max) l0p_max = et;
      }
      else if(id == 2){ // L0Hadron
        if(et > l0h_max) l0h_max = et;
      }
    }
    // roughly this is L0Hadron
    if(l0h_max > 4000 && data.spd < 450) nL0hadron++;

    // below these (+ potentially data.spd < 450) would use only events
    // passing L0Electron
    //if(l0e_max < 912) continue;
    //if(l0e_max < 2700) continue;

    int ntag = data.tag_e->size(); // number of dielectrons
    int nprt = data.prt_e->size(); // number of total particles stored
    int nmctag = 0, mcprt0, mcprt1, reco0=0, reco1=0, pass0=0, pass1=0;

    // get true J/psi pt if this is MC
    double mcjpt=-1;
    if(mc){
      nmctag = data.mctag_e->size();
      if(nmctag < 1) continue; // shouldn't ever happen
      // just use the 1st J/psi since there is almost never > 1
      // indices to the true e+ and e- at generator level
      mcprt0 = data.mctag_prt0->at(0);
      mcprt1 = data.mctag_prt1->at(0);
      mcjpt = data.getMCJpsiP4().Pt()/1000; // true J/psi pt in GeV
    }

    // loop over particles to fill some electron PID histograms
    for(int i=0; i<nprt; i++){
      TLorentzVector p4 = data.getPrtP4(i);
      if(!fid(data,i)) continue;
      // we store 2 versions of particles if brem can be added, only use the
      // ones without brem added for Hlt1 studies
      if(data.prt_brem->at(i) > 0) continue;

      double mcpt; int gen;
      if(mc){
        gen = data.prt_gen->at(i);
        // check if this reco particle is truth matched to e+ or e-
        if(gen != mcprt0 && gen != mcprt1) continue;
        if(gen == mcprt0) reco0++;
        if(gen == mcprt1) reco1++;
        mcpt = data.getMCPrtP4(gen).Pt()/1000;
      }

      double d,r,x,y; int idx;
      closestEcal(data,i,d,r,x,y,idx);
      int pass = passL0EID(data,i,idx);

      hd.Fill(d);
      hr.Fill(r);
      hdr.Fill(d,r);
      hxy.Fill(x,y);

      if(mc){
        hpt.Fill(mcpt);
        if(pass == 2) {
          hptp.Fill(mcpt);
          if(gen == mcprt0) pass0++;
          if(gen == mcprt1) pass1++;
        }
        if(pass > 0) hptp2.Fill(mcpt);
      }
    }

    // loop over dielectrons ("tags" in philtenese)
    int ntagpass=0;
    for(int i=0; i<ntag; i++){
      if(data.tag_brem->at(i) > 0) continue; // only use Hlt1-typle dilectrons
      TLorentzVector p4 = data.getTagP4(i);
      // indices to prt array of e+ and e-
      int prt0 = data.tag_prt0->at(i);
      int prt1 = data.tag_prt1->at(i);
      int l0idx0,l0idx1;
      if(!fid(data,prt0)) continue;
      if(!fid(data,prt1)) continue;
      // require both pass tight L0-based electron PID
      if(passL0EID(data,prt0,l0idx0) < 2) continue;
      if(passL0EID(data,prt1,l0idx1) < 2) continue;

      /*
      // consider displacement (checked: almost all candidates are prompt)
      double min_ipchi2 = data.prt_ipchi2->at(prt0);
      if(data.prt_ipchi2->at(prt1) < data.prt_ipchi2->at(prt0)) min_ipchi2 = data.prt_ipchi2->at(prt1);
      if(data.tag_fdchi2->at(i) > 45) continue;
      if(data.prt_ipchi2->at(prt0) > 6) continue;
      if(data.prt_ipchi2->at(prt1) > 6) continue;
      */

      // distance between first track hits in velo is a good way to kill
      // photon conversions (e.g. require this distance > 0.1 mm)
      /*
      TVector3 h0 = data.getFirstHit(prt0);
      TVector3 h1 = data.getFirstHit(prt1);
      double dist01 = (h0-h1).Mag();
      */

      // find brem and correct the mass
      TLorentzVector p0 = data.getPrtP4(prt0);
      TLorentzVector p1 = data.getPrtP4(prt1);
      double mtmp = (p0+p1).M(), min0, min1;
      double ebrem0 = findBrem(data,prt0,l0idx0,l0idx1,min0);
      double ebrem1 = findBrem(data,prt1,l0idx0,l0idx1,min1);
      p0.SetPtEtaPhiM(p0.Pt()*(p0.E()+ebrem0)/p0.E(),p0.Eta(),p0.Phi(),p0.M());
      p1.SetPtEtaPhiM(p1.Pt()*(p1.E()+ebrem1)/p1.E(),p1.Eta(),p1.Phi(),p1.M());
      double dm = (p0+p1).M() - mtmp; // can't rerun the vertex fit here

      if(!mc || data.tag_gen->at(i) >=0) {
        hm.Fill(p4.M());
        hmb.Fill(p4.M() + dm);
        hdb.Fill(min0);
        hdb.Fill(min1);
      }
      // for now, consider anything with 2 tight electrons as a pass
      ntagpass++;

      // now look for pi0,eta --> e+e-gamma using remaining L0 clusters for
      // the photon
      if(p4.M() + dm > 500) continue;
      int nl0 = data.nl0();
      for(int i=0; i<nl0; i++){
        double d = ecalDist(data,i,prt0,prt1);
        if(d < 3) continue;
        TLorentzVector pg;
        TVector3 hit(data.l0_x->at(i),data.l0_y->at(i),data.l0_z->at(i));
        pg.SetPtEtaPhiM(data.l0_et->at(i),hit.Eta(),hit.Phi(),0);
        hmeeg.Fill((p0+p1+pg).M());
      }
    }

    if(ntagpass > 0) {
      // number of dielectrons, no L0 requriement
      n1_pass++;
      // also require loose L0Electron
      if(l0e_max > 912 && data.spd < 450) n2_pass++;
      // also require standard L0Electron
      if(l0e_max > 2700 && data.spd < 450) n3_pass++;
    }

    if(mc){
      if(reco0 < 1 || reco1 < 1) continue; // e+ and/or e- not reconstructed
      hjpt.Fill(mcjpt);
      if(pass0 > 0 && pass1 > 0) hjptp.Fill(mcjpt); // both pass tight
      if(pass0 > 0 || pass1 > 0) hjptp2.Fill(mcjpt); // at least one passes
    }
  }

  // assume that L0Hadron rate is 700 kHz (TODO: check this)
  if(!mc){
    cout << "rate1 = " << n1_pass/(double)nL0hadron *700 << " kHz" << endl;
    cout << "rate2 = " << n2_pass/(double)nL0hadron *700 << " kHz"  << endl;
    cout << "rate3 = " << n3_pass/(double)nL0hadron *700 << " kHz"  << endl;
  }

  TFile *fo;
  if(mc) fo = new TFile("mc.root","recreate");
  else fo = new TFile("data.root","recreate");
  fo->cd();
  hd.Write();
  hr.Write();
  hdr.Write();
  hm.Write();
  hmb.Write();
  hxy.Write();
  hdb.Write();
  hmeeg.Write();
  if(mc){
    divide(hptp,hpt);
    divide(hptp2,hpt);
    divide(hjptp,hjpt);
    divide(hjptp2,hjpt);
    hptp.Write();
    hptp2.Write();
    hjptp.Write();
    hjptp2.Write();
  }
  delete fo;

  return 0;
}
