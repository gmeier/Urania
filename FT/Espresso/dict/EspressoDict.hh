#ifndef DICT_ESPRESSO_ESPRESSODICT_H 
#define DICT_ESPRESSO_ESPRESSODICT_H 1

#include "Espresso/GLMBuilder.hh"
#include "Espresso/RooGLMFunction.hh"
#include "Espresso/GLMNSpline.hh"
#include "Espresso/GLMBSpline.hh"
#include "Espresso/GLMPolynomial.hh"
#include "Espresso/GSLWrappers.hh"

#endif // DICT_ESPRESSO_ESPRESSODICT_H
